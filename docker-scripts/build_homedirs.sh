#!/bin/bash
INPUT=${PWD}/mapping-00
echo $INPUT
OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }

while read port pw
do
  mkdir -p ${PWD}/homedirs/$port
  cp -a  ${PWD}/homedir_template/. ${PWD}/homedirs/$port 
done < $INPUT