#!/bin/bash
#
# which port are we targetting?
#
if [ $# -ne 1 ]; then
    echo "ERROR: missing port paramater"
    exit 1
fi
TARGETPORT=$1

#
# get the server HOSTID
#
INPUT=${PWD}/hostID
OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "ERROR: $INPUT file not found"; exit 99; }
while read hostID
do
  MYHOSTID=$hostID
done < $INPUT
IFS=$OLDIFS
echo "HERE IS $MYHOSTID"
#
# loop through the user mappings
#
INPUT=${PWD}/mapping-$MYHOSTID
OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "ERROR: $INPUT file not found"; exit 99; }
while read port pw
do
  if [ "$port" == "$TARGETPORT" ] ; then
    SSH_PORT=$(($port-10000))

    echo "restarting bash-web-$port and bash-ssh-$SSH_PORT"

    sudo docker kill bash-web-$port
    sudo docker kill bash-ssh-$SSH_PORT
    sudo docker rm bash-web-$port
    sudo docker rm bash-ssh-$SSH_PORT

    docker run -d \
      --env USER_PASSWORD=$pw \
      --name bash-web-$port \
      -p 127.0.0.1\:\:8080 \
      -e VIRTUAL_HOST=localhost\:$port \
      -e USER_PASSWORD=$pw \
      -e VIRTUAL_PORT=$port \
      -e MAP_VIRTUAL_PORT=$port \
      -v ${PWD}/homedirs/$port:/home/term \
      -t cmgr-bash 

      sleep 0.5

      ### Create the SSH based container ###
      #note how we map to the same volume as the web container
      echo "SSH Container: $SSH_PORT is , PW is $pw"
      docker run -d \
      --name bash-ssh-$SSH_PORT \
      -p $SSH_PORT:22\
      -e USER_PASSWORD=$pw \
      -v ${PWD}/homedirs/$port:/home/term \
      -t cmgr-bash 

      sleep 0.5
    exit 0
  fi
done < $INPUT
echo "ERROR: cannot find a user mapping entry for port $TARGETPORT"
exit 99
IFS=$OLDIFS


