Note: "ITSO, these are test keys and password for demo purposes with no value :) " 


### Run Container Examples
1. cd `src/wetty` 
2. `./build` this will build the image
3. cd into `docker-scripts`
4. `./build_homedirs.sh` this will build initial home diretories form the `homedir_template`
5. `./run-everything` this will spin up the containers in this order
  * NGINX 
  * Docker-Gen
  * Web Based Container
  * SSH Based Container
3. In your browser go to https://localhost:40000/ssh/term?pass=badpassword0
4. To ssh, take whatever the browser port is, and subtract 10000
  * `ssh term@localhost -p 30000`
  * password: badpassword0

### Run Local
1. cd into `src/wetty`
2. `docker-compose build`
3. `docker-compose up`
  * *NOTE make sure you start it this way everytime*
  * *your local volume will be created in src/wetty/homedir*
4. In your browser, go to http://localhost:8080/wetty/ssh/term?pass=term
5. In order to ssh, run `ssh term@localhost -p 9022`
6. username term, password test_password (note these are just credentials for testing locally)
7. to stop the container `ctrl+c` then `docker-compose down` (remember to be in the `src/wetty` dir)